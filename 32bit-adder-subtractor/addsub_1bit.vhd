library ieee;
use ieee.std_logic_1164.all;

ENTITY addsub_1bit IS
    PORT (
        in_0 : IN std_logic ;
        in_1 : IN std_logic ;
        cin : IN std_logic ;
        AddOrSub : IN std_logic ; -- decide to add ( '1 ') or sub ( '0 ') operation
        sum_sub : OUT std_logic ; -- if sub operation is chosen it will be sub
        cout_bout : OUT std_logic -- if sub operation is chosen it will be borrow out
        );
END addsub_1bit ;

ARCHITECTURE beh_addsub_1bit of addsub_1bit is 
signal s1,s2,s3,prod1,
       s4,s5,s6,prod2,
       s7,s8,s9,s10,prod3,
       s11,s12,s13,s14,prod4, ----answer variable bits
       c1,c2,c3,cprod1,
       c4,c5,cprod2,
       c6,c7,cprod3,
       c8,c9,cprod4,          ----cout_bout variable bits
       c10,c11,c12,c13,c14, cprod5  : std_logic;
       
begin
-------------------------------------answer output bit
s1 <= not in_0 after 3ns;
s2 <= not in_1 after 3ns;
s3 <= cin;
prod1 <= s1 and s2 and s3 after 8ns;

s4 <= not in_0 after 3ns;
s5 <= in_1;
s6 <= not cin after 3ns;
prod2 <= s4 and s5 and s6 after 8ns;
       
s7 <= in_0;
s8 <= in_1;
s9 <= cin;
prod3 <= s7 and s8 and s9 after 3ns;

s10 <= in_0;
s11 <= not in_1 after 3ns;
s12 <= not cin after 3ns;
prod4 <= s10 and s11 and s12 after 8ns;

------------------------------------carry or borrow output bit
c1 <= not AddOrSub after 3ns;
c2 <= cin;
c3 <= not in_0 after 3ns;
cprod1 <= c1 and c2 and c3 after 8ns;

c4 <= not AddOrSub after 3ns; 
c5 <= not in_0 after 3ns;
c6 <= in_1;
cprod2 <= c4 and c5 and c6 after 8ns;

c7 <= AddOrSub;
c8 <= in_0;
c9 <= in_1;
cprod3 <= c7 and c8 and c9 after 8ns;

c10 <= AddOrSub;
c11 <= cin;
c12 <= in_0;
cprod4 <= c10 and c11 and c12 after 8ns;

c13 <= cin;
c14 <= in_1;
cprod5 <= c13 and c14 after 8ns;
-------------------------------------finalize outputs

sum_sub <= prod1 or prod2 or prod3 or prod4 after 8ns;
cout_bout <= cprod1 or cprod2 or cprod3 or cprod4 or cprod5 after 8ns;

end beh_addsub_1bit;