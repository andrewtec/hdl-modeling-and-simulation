library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_adder_sub_32 is
end tb_adder_sub_32;

architecture beh_tb_adder_sub of tb_adder_sub_32 is

constant THIRTYTWO : integer:= 32;
signal tb_a, tb_b, answer : std_logic_vector (THIRTYTWO-1 downto 0);
signal tb_cin, addorsub, tb_cout,  tb_ov : std_logic; 

component adder_sub32 is
    GENERIC (BW : INTEGER :=32);
    PORT ( a_32 : IN STD_LOGIC_VECTOR (BW -1 downto 0);
        b_32 : IN STD_LOGIC_VECTOR (BW -1 downto 0);
        cin : IN STD_LOGIC ;
        sub : IN STD_LOGIC ;            -- decide to add ( '1 ') or sub ( '0 ') operation
        sum_32 : out STD_LOGIC_VECTOR (BW -1 downto 0);
        cout : INOUT STD_LOGIC ;
        ov : OUT STD_LOGIC ); -- ov stands for overflow
end component;

begin
testValues: adder_sub32
    generic map ( BW => THIRTYTWO)
    port map ( tb_a, tb_b, tb_cin, addorsub, answer, tb_cout, tb_ov);
    
process
begin
    
--add 31 and 31
    tb_a <= "00000000000000000000000000011111";
    tb_b <=  "00000000000000000000000000011111";
    tb_cin <= '0';
    addorsub <= '1';
    wait for 4000ns;

--subtract 31 and 31
    tb_a <=  "00000000000000000000000000011111";
    tb_b <= "00000000000000000000000000011111";
    tb_cin <= '0';
    addorsub <= '0';
    wait for 4000ns;

--subtract -1 (twos comp) and 1
    tb_a <= "11111111111111111111111111111111";
    tb_b <= "00000000000000000000000000000001";
    tb_cin <= '0';
    addorsub <= '0';
    wait for 4000ns;

--test overflow (add)
    tb_a <= "01111111111111111111111111111111";
    tb_b <= "00000000000000000000000000000001";
    tb_cin <= '0';
    addorsub <= '1';
    wait for 4000ns;
    
--test cout (add negative numbers)
        tb_a <= "10000000000000000000000000000000";
        tb_b <= "10000000000000000000000000000000";
        tb_cin <= '0';
        addorsub <= '1';
        wait for 4000ns;
    
end process;
end architecture beh_tb_adder_sub;