-----------------------> Code by: Andrew Tec<---------------------------------
------------------->University of California, Irvine<----------------------
library IEEE;
use ieee.std_logic_1164.all;

ENTITY comp2 is
port (x0 : IN std_logic ;            --x0
    y0 : IN std_logic ;                  --y0
    x1 : IN std_logic ;                  --x1
    y1 : IN std_logic ;                  --y1
    great : OUT std_logic ;
    less : OUT std_logic );
END ;

ARCHITECTURE comp2_arch OF comp2 IS

signal net1, net2, net3, net4, net5, net6, gr, ls : std_logic;
begin

--handle greater
net1 <= x1 AND NOT y1 after 11ns;
net2 <= x0 AND NOT y1 after 11ns;
net3 <= x1 AND x0 after 8ns;

--handle lesser
net4 <= NOT x1 AND y1 after 11ns;
net5 <= NOT x1 AND y0 after 11ns;
net6 <= y1 AND y0 after 8ns;

gr <= net1 OR net2 OR net3 after 16ns;      --3-input OR gate is made up of 2 2-input OR gates
ls <= net4 OR net5 OR net6 after 16ns;

great <= gr;
less <= ls;

END ARCHITECTURE ;