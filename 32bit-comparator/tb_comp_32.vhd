-----------------------> Code by: Andrew Tec<---------------------------------
------------------->University of California, Irvine<----------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_comp32 is
end tb_comp32;

architecture beh_tb_comp32 of tb_comp32 is

constant THIRTYTWO : integer  := 32;
signal tb_a, tb_b  : std_logic_vector (THIRTYTWO-1 downto 0);
signal g_tb, l_tb, e_tb : std_logic ;

component comp32 is
    GENERIC (BW : INTEGER :=32);
    PORT ( a_32 : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
        b_32 : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
        g_32 : OUT STD_LOGIC ;
        l_32 : OUT STD_LOGIC ;
        e_32 : OUT STD_LOGIC );
end component;

begin
thirtytwobits: comp32 
    generic map (BW => THIRTYTWO)
    port map ( tb_a, tb_b, g_tb, l_tb, e_tb);

process
begin
    tb_a <= "11111111111111111111111111111111";
    tb_b <= "11111111111111111111111111111111";
    wait for 3000ns;
    
    tb_a <="11111111111111111111111111111110";
    tb_b <= "11111111111111111111111111111111";
    wait for 3000ns;
    
    tb_a <= "11111111111111111111111111111111";
    tb_b <= "11111111111111111111111111111110";
    wait for 3000ns;    

end process;
end architecture beh_tb_comp32;