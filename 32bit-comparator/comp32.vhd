-----------------------> Code by: Andrew Tec<---------------------------------
------------------->University of California, Irvine<----------------------

library ieee;
use ieee.std_logic_1164.all;

ENTITY comp32 is
GENERIC (BW : INTEGER :=32);
PORT ( a_32 : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
    b_32 : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
    g_32 : OUT STD_LOGIC ;
    l_32 : OUT STD_LOGIC ;
    e_32 : OUT STD_LOGIC );
END ;

ARCHITECTURE comp32_arch OF comp32 IS

component comp2         --2 bit comparator
port (x0 : IN std_logic ;            
    y0 : IN std_logic ;                  
    x1 : IN std_logic ;                  
    y1 : IN std_logic ;                  
    great : OUT std_logic ;
    less : OUT std_logic
);
end component;

type stdlogicArray is array (BW-1 downto 0) of std_logic;
signal gWire : stdlogicArray;
signal lWire : stdlogicArray;

signal fix1, fix2 : std_logic;      --for adjustments box
signal equalnet : std_logic;        --handles equals

begin

intialCompare: comp2 port map (
    a_32(0),
    b_32(0),
    a_32(1),
    b_32(1),
    gWire(0),
    lWire(0)
    );
    
RestOfBits: for i in 1 to BW-1 generate
    RippleCompare: comp2 port map(
        gWire(i-1),
        lWire(i-1),
        a_32(i),
        b_32(i),
        gWire(i),
        lWire(i)
        );
end generate;

--ADJUSTMENT BOX  (truth table fix)     REDUCES AMOUNTS OF GATES IN EACH COMPARATOR
fix1 <= gWire(BW-1) AND NOT lWire(BW-1) after 11ns;     --greater
fix2 <= lWire(BW-1) AND NOT gWire(BW-1) after 11ns;     --lesser

--HANDLES EQUALS
equalnet <= NOT(fix1 OR fix2) after 11ns;

g_32 <= fix1;
l_32 <= fix2;
e_32<= equalnet;        --1 if equal, 0 if not

END ARCHITECTURE ;