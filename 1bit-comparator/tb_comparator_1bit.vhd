library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_comparator_1bit is
end tb_comparator_1bit;

architecture beh_comparator_1bit of tb_comparator_1bit is
signal i0, i1, g, l, eq : std_logic;

component comparator_1bit is
    PORT (
    in_0 : IN STD_LOGIC ;
    in_1 : IN STD_LOGIC ;
    greater : OUT STD_LOGIC ;
    equal : OUT STD_LOGIC ;
    less : OUT STD_LOGIC
    );
end component;
begin

PortForward: comparator_1bit port map ( in_0 => i0, in_1 => i1, greater => g, less => l, equal => eq ); --map
process
begin
    i0 <= '0';
    i1 <= '0'; --MSB
    wait for 50ns;
    
    i0 <= '1';
    i1 <= '0';
    wait for 50ns;
    
    i0 <= '0';
    i1 <= '1';
    wait for 50ns;
    
    i0 <= '1';
    i1 <= '1';
    wait for 50ns;

end process;
end beh_comparator_1bit;