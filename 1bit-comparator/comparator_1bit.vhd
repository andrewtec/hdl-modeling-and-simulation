library ieee;
use ieee.std_logic_1164.all;

ENTITY comparator_1bit IS
    PORT (
        in_0 : IN STD_LOGIC ;
        in_1 : IN STD_LOGIC ; --MSB
        greater : OUT STD_LOGIC ;
        equal : OUT STD_LOGIC ;
        less : OUT STD_LOGIC
        );
END comparator_1bit ;

ARCHITECTURE beh_comparator_1bit of comparator_1bit is
signal greater_wire, less_wire : std_logic;

begin
greater_wire <= not in_0 and in_1 after 11ns;
less_wire <= not in_1 and in_0 after 11ns;

greater <= greater_wire;
less <= less_wire;
equal <= not ( greater_wire or less_wire) after 11ns;

end beh_comparator_1bit;