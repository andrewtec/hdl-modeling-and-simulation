
---------------------------------------------------------------------
-- TITLE: Register Bank
-- DESCRIPTION:
--    Implements a register bank with 32 registers that are 32-bits wide.
--    There are two read-ports and one write port.
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.c31L_pack.all;

entity reg_bank is
   port(clk            : in  std_logic;
        rs_index       : in  std_logic_vector(reg_field-1 downto 0);
        rt_index       : in  std_logic_vector(reg_field-1 downto 0);
        rd_index       : in  std_logic_vector(reg_field-1 downto 0);
		we:	in std_logic;
        reg_source_out : out std_logic_vector(BW-1 downto 0);
        reg_target_out : out std_logic_vector(BW-1 downto 0);
        reg_dest_new   : in  std_logic_vector(BW-1 downto 0));
        
end; --entity reg_bank

architecture logic of reg_bank is
signal reg01: std_logic_vector(BW-1 downto 0):=x"00000001";
signal reg03: std_logic_vector(BW-1 downto 0):=x"00000003";

   signal reg31,  reg02 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg04, reg05, reg06, reg07 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg08, reg09, reg10, reg11 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg12, reg13, reg14, reg15 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg16, reg17, reg18, reg19 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg20, reg21, reg22, reg23 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg24, reg25, reg26, reg27 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg28, reg29, reg30        : std_logic_vector(BW-1 downto 0):=(others=>'0');
   
   signal reg32, reg33, reg34,reg35,reg36,reg37,reg38,reg39,reg40 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg41,reg42,reg43,reg44,reg45,reg46,reg47,reg48,reg49,reg50 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg51,reg52,reg53,reg54,reg55,reg56,reg57,reg58,reg59,reg60,reg61,reg62,reg63 : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg_epc                    : std_logic_vector(BW-1 downto 0):=(others=>'0');
   signal reg_status                 : std_logic;
begin

reg_proc: process(clk, rs_index, rt_index, rd_index, reg_dest_new,
   reg31, reg01, reg02, reg03, reg04, reg05, reg06, reg07,
   reg08, reg09, reg10, reg11, reg12, reg13, reg14, reg15,
   reg16, reg17, reg18, reg19, reg20, reg21, reg22, reg23,
   reg24, reg25, reg26, reg27, reg28, reg29, reg30,reg32, reg33, reg34,reg35,reg36,reg37,reg38,reg39,reg40,
   reg41,reg42,reg43,reg44,reg45,reg46,reg47,reg48,reg49,reg50,reg51,reg52,reg53,reg54,reg55,reg56,reg57,reg58,reg59,reg60,reg61,reg62,reg63,
   reg_epc, reg_status)
begin
   case rs_index is
   when "000000" => reg_source_out <= ZERO;
   when "000001" => reg_source_out <= reg01;
   when "000010" => reg_source_out <= reg02;
   when "000011" => reg_source_out <= reg03;
   when "000100" => reg_source_out <= reg04;
   when "000101" => reg_source_out <= reg05;
   when "000110" => reg_source_out <= reg06;
   when "000111" => reg_source_out <= reg07;
   when "001000" => reg_source_out <= reg08;
   when "001001" => reg_source_out <= reg09;
   when "001010" => reg_source_out <= reg10;
   when "001011" => reg_source_out <= reg11;
   when "001100" => reg_source_out <= reg12;
   when "001101" => reg_source_out <= reg13;
   when "001110" => reg_source_out <= reg14;
   when "001111" => reg_source_out <= reg15;
   when "010000" => reg_source_out <= reg16;
   when "010001" => reg_source_out <= reg17;
   when "010010" => reg_source_out <= reg18;
   when "010011" => reg_source_out <= reg19;
   when "010100" => reg_source_out <= reg20;
   when "010101" => reg_source_out <= reg21;
   when "010110" => reg_source_out <= reg22;
   when "010111" => reg_source_out <= reg23;
   when "011000" => reg_source_out <= reg24;
   when "011001" => reg_source_out <= reg25;
   when "011010" => reg_source_out <= reg26;
   when "011011" => reg_source_out <= reg27;
   when "011100" => reg_source_out <= reg28;
   when "011101" => reg_source_out <= reg29;
   when "011110" => reg_source_out <= reg30;
   when "011111" => reg_source_out <= reg31;
   when "100000" => reg_source_out <= reg32;
   when "100001" => reg_source_out <= reg33;
   
   when "100010" => reg_source_out <= reg34;
   when "100011" => reg_source_out <= reg35;
   when "100100" => reg_source_out <= reg36;
   when "100101" => reg_source_out <= reg37;
   when "100110" => reg_source_out <= reg38;
   when "100111" => reg_source_out <= reg39;
   when "101000" => reg_source_out <= reg40;
   when "101001" => reg_source_out <= reg41;
   when "101010" => reg_source_out <= reg42;
   when "101011" => reg_source_out <= reg43;
   when "101100" => reg_source_out <= reg44;
   when "101101" => reg_source_out <= reg45;
   when "101110" => reg_source_out <= reg46;
   when "101111" => reg_source_out <= reg47;
   when "110000" => reg_source_out <= reg48;
   when "110001" => reg_source_out <= reg49;
   when "110010" => reg_source_out <= reg50;
   when "110011" => reg_source_out <= reg51;
   when "110100" => reg_source_out <= reg52;
   when "110101" => reg_source_out <= reg53;
   when "110110" => reg_source_out <= reg54;
   when "110111" => reg_source_out <= reg55;
   when "111000" => reg_source_out <= reg56;
   when "111001" => reg_source_out <= reg57;
   when "111010" => reg_source_out <= reg58;
   when "111011" => reg_source_out <= reg59;
   when "111100" => reg_source_out <= reg60;
   when "111101" => reg_source_out <= reg61;
   when "111110" => reg_source_out <= reg62;
   when "111111" => reg_source_out <= reg63;
   
  -- when "101100" => reg_source_out <= ZERO(31 downto 1) & reg_status;
   --when "101110" => reg_source_out <= reg_epc;     --CP0 14
   --when "111111" => reg_source_out <= '1' & ZERO(30 downto 0); --intr vector
   when others =>   reg_source_out <= ZERO;
   end case;

   case rt_index is
   when "000000" => reg_target_out <= ZERO;
   when "000001" => reg_target_out <= reg01;
   when "000010" => reg_target_out <= reg02;
   when "000011" => reg_target_out <= reg03;
   when "000100" => reg_target_out <= reg04;
   when "000101" => reg_target_out <= reg05;
   when "000110" => reg_target_out <= reg06;
   when "000111" => reg_target_out <= reg07;
   when "001000" => reg_target_out <= reg08;
   when "001001" => reg_target_out <= reg09;
   when "001010" => reg_target_out <= reg10;
   when "001011" => reg_target_out <= reg11;
   when "001100" => reg_target_out <= reg12;
   when "001101" => reg_target_out <= reg13;
   when "001110" => reg_target_out <= reg14;
   when "001111" => reg_target_out <= reg15;
   when "010000" => reg_target_out <= reg16;
   when "010001" => reg_target_out <= reg17;
   when "010010" => reg_target_out <= reg18;
   when "010011" => reg_target_out <= reg19;
   when "010100" => reg_target_out <= reg20;
   when "010101" => reg_target_out <= reg21;
   when "010110" => reg_target_out <= reg22;
   when "010111" => reg_target_out <= reg23;
   when "011000" => reg_target_out <= reg24;
   when "011001" => reg_target_out <= reg25;
   when "011010" => reg_target_out <= reg26;
   when "011011" => reg_target_out <= reg27;
   when "011100" => reg_target_out <= reg28;
   when "011101" => reg_target_out <= reg29;
   when "011110" => reg_target_out <= reg30;
   when "011111" => reg_target_out <= reg31;
   
   when "100000" => reg_target_out <= reg32;
   when "100001" => reg_target_out <= reg33;
   when "100010" => reg_target_out <= reg34;
   when "100011" => reg_target_out <= reg35;
   when "100100" => reg_target_out <= reg36;
   when "100101" => reg_target_out <= reg37;
   when "100110" => reg_target_out <= reg38;
   when "100111" => reg_target_out <= reg39;
   when "101000" => reg_target_out <= reg40;
   when "101001" => reg_target_out <= reg41;
   when "101010" => reg_target_out <= reg42;
   when "101011" => reg_target_out <= reg43;
   when "101100" => reg_target_out <= reg44;
   when "101101" => reg_target_out <= reg45;
   when "101110" => reg_target_out <= reg46;
   when "101111" => reg_target_out <= reg47;
   when "110000" => reg_target_out <= reg48;
   when "110001" => reg_target_out <= reg49;
   when "110010" => reg_target_out <= reg50;
   when "110011" => reg_target_out <= reg51;
   when "110100" => reg_target_out <= reg52;
   when "110101" => reg_target_out <= reg53;
   when "110110" => reg_target_out <= reg54;
   when "110111" => reg_target_out <= reg55;
   when "111000" => reg_target_out <= reg56;
   when "111001" => reg_target_out <= reg57;
   when "111010" => reg_target_out <= reg58;
   when "111011" => reg_target_out <= reg59;
   when "111100" => reg_target_out <= reg60;
   when "111101" => reg_target_out <= reg61;
   when "111110" => reg_target_out <= reg62;
   when "111111" => reg_target_out <= reg63;
   
   when others =>   reg_target_out <= ZERO;
   end case;

   if (rising_edge(clk) and we='1') then
--      assert reg_dest_new'last_event >= 10 ns
--         report "Reg_dest timing error";
      case rd_index is
      when "000001" => reg01 <= reg_dest_new;
      when "000010" => reg02 <= reg_dest_new;
      when "000011" => reg03 <= reg_dest_new;
      when "000100" => reg04 <= reg_dest_new;
      when "000101" => reg05 <= reg_dest_new;
      when "000110" => reg06 <= reg_dest_new;
      when "000111" => reg07 <= reg_dest_new;
      when "001000" => reg08 <= reg_dest_new;
      when "001001" => reg09 <= reg_dest_new;
      when "001010" => reg10 <= reg_dest_new;
      when "001011" => reg11 <= reg_dest_new;
      when "001100" => reg12 <= reg_dest_new;
      when "001101" => reg13 <= reg_dest_new;
      when "001110" => reg14 <= reg_dest_new;
      when "001111" => reg15 <= reg_dest_new;
      when "010000" => reg16 <= reg_dest_new;
      when "010001" => reg17 <= reg_dest_new;
      when "010010" => reg18 <= reg_dest_new;
      when "010011" => reg19 <= reg_dest_new;
      when "010100" => reg20 <= reg_dest_new;
      when "010101" => reg21 <= reg_dest_new;
      when "010110" => reg22 <= reg_dest_new;
      when "010111" => reg23 <= reg_dest_new;
      when "011000" => reg24 <= reg_dest_new;
      when "011001" => reg25 <= reg_dest_new;
      when "011010" => reg26 <= reg_dest_new;
      when "011011" => reg27 <= reg_dest_new;
      when "011100" => reg28 <= reg_dest_new;
      when "011101" => reg29 <= reg_dest_new;
      when "011110" => reg30 <= reg_dest_new;
      when "011111" => reg31 <= reg_dest_new;
	  
	  when "100000" => reg32 <= reg_dest_new;
      when "100001" => reg33 <= reg_dest_new;
      when "100010" => reg34 <= reg_dest_new;
	  when "100011" => reg35 <= reg_dest_new;
      when "100100" => reg36 <= reg_dest_new;
      when "100101" => reg37 <= reg_dest_new;
      when "100110" => reg38 <= reg_dest_new;
      when "100111" => reg39 <= reg_dest_new;
      when "101000" => reg40 <= reg_dest_new;
      when "101001" => reg41 <= reg_dest_new;
      when "101010" => reg42 <= reg_dest_new;
      when "101011" => reg43 <= reg_dest_new;
      when "101100" => reg44 <= reg_dest_new;
      when "101101" => reg45 <= reg_dest_new;
      when "101110" => reg46 <= reg_dest_new;
      when "101111" => reg47 <= reg_dest_new;
      when "110000" => reg48 <= reg_dest_new;
      when "110001" => reg49 <= reg_dest_new;
      when "110010" => reg50 <= reg_dest_new;
      when "110011" => reg51 <= reg_dest_new;
      when "110100" => reg52 <= reg_dest_new;
      when "110101" => reg53 <= reg_dest_new;
      when "110110" => reg54 <= reg_dest_new;
      when "110111" => reg55 <= reg_dest_new;
      when "111000" => reg56 <= reg_dest_new;
      when "111001" => reg57 <= reg_dest_new;
      when "111010" => reg58 <= reg_dest_new;
      when "111011" => reg59 <= reg_dest_new;
      when "111100" => reg60 <= reg_dest_new;
      when "111101" => reg61 <= reg_dest_new;
      when "111110" => reg62 <= reg_dest_new;
      when "111111" => reg63 <= reg_dest_new;
	  
	  
      --when "101100" => reg_status <= reg_dest_new(0);
      --when "101110" => reg_epc <= reg_dest_new;  --CP0 14
                       reg_status <= '0';        --disable interrupts
      when others =>
      end case;
   end if;
   --intr_enable <= reg_status;
end process;

end; --architecture logic
