library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.c31L_pack.all;

ENTITY control IS
    PORT ( instruction : in STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
        rt_concat_imm : out STD_LOGIC_VECTOR (14 DOWNTO 0);
        write : out STD_LOGIC ;
        rs_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        rt_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        rd_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        b_mux_sel : out STD_LOGIC ;
        alu_func : out alu_function_type );
end ; -- entity control

architecture beh_control of control is 

signal func : alu_function_type;

begin

func <= instruction(BW-14  downto BW-17);

b_mux_sel <= instruction(BW-1);
rs_index <= instruction(BW-2 downto BW-7);
rd_index <= instruction(BW-8 downto BW-13);
alu_func <= func;
rt_index <= instruction (BW-18 downto BW-23);
rt_concat_imm <= instruction(BW-18 downto 0);

write <= '0' when ((func = "0000") or (func = "0011")) else '1';      --not writing when alu_nop, alu_comp

end architecture beh_control;