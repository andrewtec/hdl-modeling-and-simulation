library ieee;
use ieee.std_logic_1164.all;
use work.c31L_pack.all;

ENTITY processor IS
    PORT ( 
        clk : in STD_LOGIC ;
        instruction : in STD_LOGIC_VECTOR (BW -1 downto 0);
        great : out STD_LOGIC ;
        less : out STD_LOGIC ;
        equal : out STD_LOGIC ;
        carry : out STD_LOGIC ;
        over_flow : out STD_LOGIC );
end processor;

ARCHITECTURE beh_processor of processor is

component control IS
    PORT ( instruction : in STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
        rt_concat_imm : out STD_LOGIC_VECTOR (14 DOWNTO 0);
        write : out STD_LOGIC ;
        rs_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        rt_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        rd_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        b_mux_sel : out STD_LOGIC ;     --'0' reg, '1' imm
        alu_func : out alu_function_type );
end component control;

component reg_bank is
   port(clk            : in  std_logic;
        rs_index       : in  std_logic_vector(reg_field-1 downto 0);
        rt_index       : in  std_logic_vector(reg_field-1 downto 0);
        rd_index       : in  std_logic_vector(reg_field-1 downto 0);
		we:	in std_logic;
        reg_source_out : out std_logic_vector(BW-1 downto 0);
        reg_target_out : out std_logic_vector(BW-1 downto 0);
        reg_dest_new   : in  std_logic_vector(BW-1 downto 0));
end component reg_bank;

component alu32 IS
    PORT ( 
        a_alu32 : in STD_LOGIC_VECTOR (BW -1 downto 0);
        b_alu32 : in STD_LOGIC_VECTOR (BW -1 downto 0);
        alu_op : in alu_function_type ;
        g : out STD_LOGIC ;
        e : out STD_LOGIC ;
        l : out STD_LOGIC ;
        o_alu32 : out STD_LOGIC_VECTOR (BW -1 downto 0);
        c_alu32 : inout STD_LOGIC ;
        ov_alu32 : out STD_LOGIC );
end component alu32;

signal instruct, alu_out, a_alu, b_alu, b_alu_dummy, reg_out, reg_target, imm : std_logic_vector (BW-1 downto 0);
signal rt_imm : std_logic_vector (14 downto 0);
signal rt_imm_fill : std_logic_vector (BW-1 downto 15);
signal reg_or_imm, write_enable, clk_behavior, carryanswer : std_logic; --reg_or_imm: '0' reg, '1' imm
signal rs, rt, rd  : std_logic_vector (reg_field -1 downto 0);
signal operation : alu_function_type;

BEGIN

instruct <= instruction;
clk_behavior <= clk;
carry <= carryanswer;
rt_imm_fill <= (others => rt_imm(14));      --sign extension
imm <= (rt_imm_fill & rt_imm);      --rt & imm with sign extention

----shift fix
--b_alu <= imm when( (operation = alu_shift_logic_left) or (operation = alu_shift_logic_right) or (operation = alu_shift_arith_left) or (operation = alu_shift_arith_right) )
--        else b_alu_dummy;
        
--normal behavior
b_alu<= reg_target when (reg_or_imm = '0') else
        imm when (reg_or_imm = '1');
        
--alu_mov/alu_movI fix            
a_alu <= (rt_imm_fill & rt_imm) when ((reg_or_imm = '1') and (operation = alu_mov)) else reg_out;


decode: control port map (
    instruction => instruct,
    rt_concat_imm => rt_imm,
    write => write_enable,
    rs_index => rs,
    rt_index => rt,
    rd_index => rd,
    b_mux_sel => reg_or_imm,    --'0' reg, '1' imm
    alu_func => operation
    );

registry_file: reg_bank port map (
    clk => clk_behavior,
    rs_index => rs,
    rt_index => rt,
    rd_index => rd,
    we => write_enable,
    reg_source_out => reg_out,
    reg_target_out => reg_target,
    reg_dest_new => alu_out
    );
    
alu: alu32 port map (
    a_alu32 => a_alu,
    b_alu32 => b_alu,
    alu_op => operation,
    g => great,
    e => equal,
    l => less,
    o_alu32 =>alu_out,
    c_alu32 => carryanswer,
    ov_alu32 => over_flow
    );

END ARCHITECTURE beh_processor;