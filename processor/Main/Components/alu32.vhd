 library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;       --typecast std_logic to integer for shifting
use work.c31L_pack.all;

ENTITY alu32 IS
    PORT ( 
        a_alu32 : in STD_LOGIC_VECTOR (BW -1 downto 0);
        b_alu32 : in STD_LOGIC_VECTOR (BW -1 downto 0);     --will also determine how many shifts
        alu_op : in alu_function_type ;
        g : out STD_LOGIC ;
        e : out STD_LOGIC ;
        l : out STD_LOGIC ;
        o_alu32 : out STD_LOGIC_VECTOR (BW -1 downto 0);
        c_alu32 : inout STD_LOGIC ;
        ov_alu32 : out STD_LOGIC );
end ;

ARCHITECTURE beh_alu32 of alu32 is

------adder/sub-------------
component adder_sub32 is
PORT ( a_32 : IN STD_LOGIC_VECTOR (BW -1 downto 0);
    b_32 : IN STD_LOGIC_VECTOR (BW -1 downto 0);
    cin : IN STD_LOGIC ;
    sub : IN STD_LOGIC ;            -- decide to add ( '1 ') or sub ( '0 ') operation
    sum_32 : out STD_LOGIC_VECTOR (BW -1 downto 0);
    cout : INOUT STD_LOGIC ;
    ov : OUT STD_LOGIC );
end component adder_sub32;

----------comparator------------
component comp32 is
PORT ( a_32 : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
    b_32 : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
    g_32 : OUT STD_LOGIC ;
    l_32 : OUT STD_LOGIC ;
    e_32 : OUT STD_LOGIC );
end component comp32;

---------shifter------------
component shifter is
PORT ( i : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
    dir : IN BIT; -- dir ='0'  shift right, dir ='1' shift left
    AL: IN BIT ; -- AL='1' means arithmetic, AL ='0' means logical
    shifting : IN INTEGER range 0 to BW -1;
    o: OUT STD_LOGIC_VECTOR (BW -1 DOWNTO 0));
end component shifter;

signal direction, arithLogic : bit;        --addsub: add is '1' -- direction: '0' is right -- arithLogic: '1' is arith
signal addsub, great, less, equal : std_logic;
signal theshifted, theanswer : std_logic_vector (BW-1 downto 0);
signal carryout, overflow : std_logic;
signal bigmux : mux_in_16;

BEGIN

----handles conditions when adder or comparator is used or unused
g <= great when alu_op = alu_comp else 'Z';
e <= equal when alu_op = alu_comp else 'Z';
l <= less when alu_op = alu_comp else 'Z';
ov_alu32 <= overflow when ((alu_op = alu_add) or (alu_op = alu_sub)) else 'Z';
c_alu32 <= carryout when ((alu_op = alu_add) or (alu_op = alu_sub)) else 'Z';

theaddersub: adder_sub32 port map (
    a_32=> a_alu32,
    b_32 => b_alu32,
    cin => '0',     --hardwired to ground; could be left 'open'
    sub => addsub,      --'0' sub, '1' add
    sum_32 => theanswer,
    cout => carryout,
    ov => overflow
    );

thecomparator: comp32 port map (
    a_32 => a_alu32,
    b_32 => b_alu32,
    g_32 => great,
    l_32 => less,
    e_32 => equal
    );
    
theshifter: shifter port map (
    i => a_alu32,
    dir => direction,       --'0' right, '1' left
    AL=> arithLogic,        --'1' arith, '0' logic
    shifting => to_integer(unsigned(b_alu32)),
    o=>theshifted
    ); 

-----fill in bigmux    
bigmux(0) <= ZERO;
bigmux(1) <= theanswer; 
bigmux(2) <= theanswer;

--handles addition or subtraction
addsub <= '1' when (alu_op = alu_add) else '0' when (alu_op = alu_sub);

bigmux(3) <= ONES when (less = '1') else ZERO when (less ='0');       --SLT

logic_operators: for i in 0 to BW-1 generate
    bigmux(4)(i) <= a_alu32(i) AND b_alu32(i);
    bigmux(5)(i) <= a_alu32(i) OR b_alu32(i);
    bigmux(6)(i) <= NOT a_alu32(i);
    bigmux(7)(i) <= (((a_alu32(i)) AND NOT (b_alu32(i))) OR ((b_alu32(i)) AND NOT (a_alu32(i)))); --XOR
end generate logic_operators;

bigmux(8) <= theshifted;
bigmux(9) <= theshifted;
bigmux(10) <= theshifted;
bigmux(11) <= theshifted;

--handles shifting direction
direction <= '0' when ((alu_op = alu_shift_logic_right) or (alu_op = alu_shift_arith_right)) else
        '1' when ((alu_op = alu_shift_logic_left) or (alu_op = alu_shift_arith_left));
--handles arith or logic
arithLogic <= '1' when ((alu_op = alu_shift_arith_right) or (alu_op = alu_shift_arith_left)) else 
    '0' when ((alu_op = alu_shift_logic_right) or (alu_op = alu_shift_logic_right));

    o_alu32 <=
    bigmux(0) when alu_op = alu_nop else
    bigmux(1) when alu_op = alu_add else
    bigmux(2) when alu_op = alu_sub else
    --when alu_comp,
    bigmux(3) when alu_op =  alu_slt else
    bigmux(4) when alu_op = alu_and else
    bigmux(5) when alu_op = alu_or else
    bigmux(6) when alu_op = alu_not else
    bigmux(7) when alu_op = alu_xor else
    bigmux(8) when alu_op = alu_shift_logic_left else
    bigmux(9) when alu_op = alu_shift_logic_right else
    bigmux(10) when alu_op = alu_shift_arith_left else
    bigmux(11) when alu_op = alu_shift_arith_right else
    a_alu32 when alu_op = alu_mov else              --rt; rs or rt? a_alu32 or b_alu32?     --fixed in processor
        (others => 'Z');

END ARCHITECTURE beh_alu32;