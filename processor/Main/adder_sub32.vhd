library ieee;
use ieee.std_logic_1164.all;
use work.c31L_pack.all;

ENTITY adder_sub32 is
PORT ( a_32 : IN STD_LOGIC_VECTOR (BW -1 downto 0);
    b_32 : IN STD_LOGIC_VECTOR (BW -1 downto 0);
    cin : IN STD_LOGIC ;
    sub : IN STD_LOGIC ;            -- decide to add ( '1 ') or sub ( '0 ') operation
    sum_32 : out STD_LOGIC_VECTOR (BW -1 downto 0);
    cout : INOUT STD_LOGIC ;
    ov : OUT STD_LOGIC ); -- ov stands for overflow
END adder_sub32;

ARCHITECTURE adder_sub32_arch OF adder_sub32 IS

type stdLogicArray is array (0 to BW-1) of std_logic;         
signal carry : stdLogicArray;                                                   --the wires to connect each carryout-carryin
  
component addsub_1bit port (
        in_0 : IN std_logic ;
        in_1 : IN std_logic ;
        cin : IN std_logic ;
        AddOrSub : IN std_logic ; -- decide to add ( '1 ') or sub ( '0 ') operation
        sum_sub : OUT std_logic ; -- if sub operation is chosen it will be sub
        cout_bout : OUT std_logic -- if sub operation is chosen it will be borrow out
        );
end component;

begin
firstFA: addsub_1bit port map (         --assign initial values (take in a carry in)
    in_0 => a_32(0),
    in_1 => b_32(0),
    cin => cin,
    AddOrSub => sub,                        
    sum_sub => sum_32(0),
    cout_bout => carry(0)
    );
    
connectEm: for i in 1 to BW-1 generate        --ripple carry adder, go through all 32 bits
    FA: addsub_1bit port map ( 
        a_32(i),
        b_32(i),
        carry(i-1),
        sub,                                                            --all AddOrSub bits is controlled by the same SUB bit                                                              
        sum_32(i),
        carry(i)
       );
end generate;

cout <= carry(BW-1);     --give a cout
ov <= (carry(BW-1) AND NOT carry(BW-2)) OR (carry(BW-2) AND NOT carry(BW-1)) after 19ns;    --overflow detection

END ARCHITECTURE ;