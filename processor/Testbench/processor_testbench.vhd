library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.c31L_pack.all;

entity processor31L_testbench is
end processor31L_testbench;

architecture processor31L_arch_testbench of processor31L_testbench is

--processor
signal tb_clk :std_logic := '0';
signal tb_great, tb_less, tb_equal, tb_carry, tb_over_flow : std_logic;
signal tb_instruction : std_logic_vector (BW-1 downto 0);
constant CLOCK_HALF_PERIOD : time := 200ns; 

component processor IS
    PORT ( 
        clk : in STD_LOGIC ;
        instruction : in STD_LOGIC_VECTOR (BW -1 downto 0);
        great : out STD_LOGIC ;
        less : out STD_LOGIC ;
        equal : out STD_LOGIC ;
        carry : out STD_LOGIC ;
        over_flow : out STD_LOGIC );
end component processor;

begin

tb_processor: processor port map (
    tb_clk,
    tb_instruction,
    tb_great,
    tb_less,
    tb_equal,
    tb_carry,
    tb_over_flow
);

clock_process: process
begin

tb_clk <= '0';
wait for CLOCK_HALF_PERIOD;
tb_clk <= '1';
wait for CLOCK_HALF_PERIOD;

end process;

instruction_process: process
begin

tb_instruction <= "1"&"000000"&"000001"&"0001"&"000000"&"000000010";        --2 + reg0 = 2 => reg1
wait for 800ns;

tb_instruction <= "1"&"000001"&"000010"&"0001"&"000000"&"000000101";        --5 + reg1 = 7 => reg2
wait for 800ns;

tb_instruction <= "0"&"000010"&"000011"&"0001"&"000001"&"000000000";        --reg1 + reg2 = 9 => reg3
wait for 800ns;

tb_instruction <= "0"&"000010"&"000100"&"0010"&"000001"&"000000000";        --reg2 - ref1 = 5 => reg4
wait for 800ns;

tb_instruction <= "0"&"000010"&"000000"&"0011"&"000001"&"000000000";    --reg2 COMP reg1 = greater      //long delay!!
wait for 2000ns;

tb_instruction <= "1"&"000011"&"000000"&"0011"&"000000"&"000001001";    --reg3 COMP 9 = comp      //long delay!!
wait for 2000ns;

tb_instruction <= "0"&"000001"&"000000"&"0011"&"000010"&"000000000";        --reg1 COMP reg2 = less     //long delay!!
wait for 2000ns;

tb_instruction <= "1"&"000001"&"000100"&"0100"&"000000"&"000000001";        --reg1 < 1 = flase = "0" => reg4    //long delay!!!     --SLT       --IMM
wait for 2000ns;

tb_instruction <= "0"&"000001"&"000011"&"0100"&"000010"&"000000000";        --reg1< reg2 = true = "1" => reg3  //long delay!!     --SLT       --reg
wait for 2000ns;

tb_instruction <= "1"&"000000"&"010110"&"1101"&"111111"&"111111111";        --MOVI: ONES=> reg22      --give ONES to check logical operators        --imm**
wait for 800ns;

tb_instruction <= "0"&"010110"&"000101"&"0101"&"000010"&"000000000";        --reg22 AND reg2 => reg5
wait for 800ns;

tb_instruction <= "1"&"010110"&"000110"&"0101"&"111110"&"111111110";        --reg22 AND "510" => reg6
wait for 800ns;

tb_instruction <= "0"&"000011"&"000111"&"0110"&"000100"&"000000000";        --reg3 OR reg4 => reg7
wait for 800ns;

tb_instruction <= "1"&"000011"&"001000"&"0110"&"111110"&"111111110";        --reg4 OR "510" => reg8
wait for 800ns;

tb_instruction <= "0"&"001000"&"001001"&"0111"&"000000"&"000000000";        --NOT reg8 => reg9
wait for 800ns;

tb_instruction <= "0"&"000100"&"001010"&"1000"&"001000"&"000000000";     --reg4 XOR reg8 => reg10
wait for 800ns;

tb_instruction <= "1"&"000100"&"001011"&"1000"&"000001"&"000000001";        --reg4 XOR rt_imm => reg11
wait for 800ns;

tb_instruction <= "0"&"000100"&"001100"&"1001"&"000000"&"000000111";        --LSL(reg0) reg4  => reg12, OP =0       --no shifting
wait for 800ns;

tb_instruction <= "1"&"000100"&"001101"&"1001"&"000000"&"000000111";        --LSL(7) reg4 => reg13, OP =1*  
wait for 800ns;

tb_instruction <= "0"&"000100"&"001110"&"1010"&"000000"&"000000111";        --LSR(reg0) reg4 => reg14, OP =0        --no shifting
wait for 800ns;

tb_instruction <= "1"&"000100"&"001111"&"1010"&"000000"&"000000111";        --LSR(7) reg4 => reg15, OP =1
wait for 800ns;

tb_instruction <= "1"&"000000"&"010000"&"1101"&"111100"&"000000000";        --MOVI: -1024 => reg16      --give negative number to check arithmetic shifts later on
wait for 800ns;

tb_instruction <= "0"&"000100"&"010001"&"1101"&"000000"&"000000000";        --MOV reg4 => reg17
wait for 800ns;

tb_instruction <= "0"&"000100"&"010010"&"1011"&"000000"&"000000111";        --ASL(reg0) reg16 => reg18   OP=0      --no shifting
wait for 800ns;

tb_instruction <= "1"&"000100"&"010011"&"1011"&"000000"&"000000111";        --ASL(7) reg16 => reg19   OP =1
wait for 800ns;

tb_instruction <= "0"&"000100"&"010100"&"1100"&"000000"&"000000111";        --ASR(reg0) reg16 => reg18   OP = 0*   --no shifting
wait for 800ns;

tb_instruction <= "1"&"000100"&"010101"&"1100"&"000000"&"000000111";        --ASR(7) reg16 => reg18   OP = 1
wait for 800ns;

tb_instruction <= "1"&"010000"&"010101"&"0000"&"000000"&"000000010";        --no operation
wait  for 800ns;

end process;

end architecture  processor31L_arch_testbench;