library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.c31L_pack.all;

entity alu32_testbench is
end alu32_testbench;

architecture alu32_arch_testbench of alu32_testbench is

signal a, b, output : std_logic_vector (BW-1 downto 0);
signal operation : alu_function_type;
signal greater, lesser, equal, carryout, overflow : std_logic;

component alu32 IS
    PORT ( 
        a_alu32 : in STD_LOGIC_VECTOR (BW -1 downto 0);
        b_alu32 : in STD_LOGIC_VECTOR (BW -1 downto 0);     --will also determine how many shifts
        alu_op : in alu_function_type ;
        g : out STD_LOGIC ;
        e : out STD_LOGIC ;
        l : out STD_LOGIC ;
        o_alu32 : out STD_LOGIC_VECTOR (BW -1 downto 0);
        c_alu32 : inout STD_LOGIC ;
        ov_alu32 : out STD_LOGIC );
end component alu32;

begin

dut: alu32 port map (
    a,
    b,
    operation,
    greater,
    equal,
    lesser,
    output,
    carryout,
    overflow
    );

process
begin

a <= "00000000000000000000000000001010";    --10
b <= "00000000000000000000000000001111";    --15
operation <= alu_nop;
wait for 1000ns;

a <= "00000000000000000000000000001010";    --10
b <= "00000000000000000000000000001111";    --15
operation <= alu_add;
wait for 1000ns;

a <= "00000000000000000000000000001010";    --10
b <= "00000000000000000000000000001111";    --15
operation <= alu_sub;
wait for 1000ns;

a <= "00000000000000000000000000001010";    --10
b <= "00000000000000000000000000001111";    --15
operation <= alu_comp;
wait for 1000ns;

a <= "00000000000000000000000000001010";    --10
b <= "00000000000000000000000000001111";    --15
operation <= alu_slt;
wait for 1000ns;

a <= "11111111111111111111111111111111";    --all ones
b <= "00000000000000000000000000001111";    --15
operation <= alu_and;
wait for 1000ns;

a <= "00000000000000000000000000000000";    -- all zeros
b <= "00000000000000000000000000001111";    --15
operation <= alu_or;
wait for 1000ns;

a <= "00000000000000000000000000000000";    --zeros
b <= "00000000000000000000000000001111";    --15
operation <= alu_not;
wait for 1000ns;

a <= "11111111111111111111111111111111";    --all ones
b <= "00000000000000000000000000001111";    --15
operation <= alu_xor;
wait for 1000ns;

a <= "11111111111111111111111111111111";    --all ones
b <= "00000000000000000000000000001111";    --15
operation <= alu_shift_logic_left;
wait for 1000ns;

a <= "11111111111111111111111111111111";    --all ones
b <= "00000000000000000000000000001111";    --15
operation <= alu_shift_logic_right;
wait for 1000ns;

a <= "11000000000000000000000000000000";    -- two ones at MSBs
b <= "00000000000000000000000000001111";    --15
operation <= alu_shift_arith_left;
wait for 1000ns;

a <= "11000000000000000000000000000000";    -- twos ones at MSBs
b <= "00000000000000000000000000001111";    --15
operation <= alu_shift_arith_right;
wait for 1000ns;

end process;
end architecture alu32_arch_testbench;
