library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.c31L_pack.all;


entity control_testbench is
end control_testbench;

architecture control_arch_testbench of control_testbench is

signal tb_instruction : std_logic_vector (BW-1 downto 0);
signal rt_imm_tb : std_logic_vector (14 downto 0);
signal write_tb, reg_or_imm : std_logic;
signal rs, rt, rd : std_logic_vector (reg_field -1 downto 0);
signal operation : alu_function_type;

component control IS
    PORT ( instruction : in STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
        rt_concat_imm : out STD_LOGIC_VECTOR (14 DOWNTO 0);
        write : out STD_LOGIC ;
        rs_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        rt_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        rd_index : out STD_LOGIC_VECTOR ( reg_field -1 DOWNTO 0);
        b_mux_sel : out STD_LOGIC ;
        alu_func : out alu_function_type );
end component control; -- entity control

begin
dut: control port map (
    tb_instruction,
    rt_imm_tb,
    write_tb,
    rs,
    rt,
    rd,
    reg_or_imm,
    operation
    );

instruction_process: process
begin

tb_instruction <= "1"&"000000"&"000001"&"0001"&"000000"&"000000010";        --2 + reg0 = 2 => reg1
wait for 800ns;

tb_instruction <= "1"&"000001"&"000010"&"0001"&"000000"&"000000101";        --5 + reg1 = 7 => reg2
wait for 800ns;

tb_instruction <= "0"&"000010"&"000011"&"0001"&"000001"&"000000000";        --reg1 + reg2 = 9 => reg3
wait for 800ns;

tb_instruction <= "0"&"000010"&"000100"&"0010"&"000001"&"000000000";        --reg2 - ref1 = 5 => reg4
wait for 800ns;

tb_instruction <= "0"&"000010"&"000000"&"0011"&"000001"&"000000000";    --reg2 COMP reg1 = greater      //long delay!!
wait for 2000ns;

tb_instruction <= "1"&"000011"&"000000"&"0011"&"000000"&"000001001";    --reg3 COMP 9 = comp      //long delay!!
wait for 2000ns;

tb_instruction <= "0"&"000001"&"000000"&"0011"&"000010"&"000000000";        --reg1 COMP reg2 = less     //long delay!!
wait for 2000ns;

tb_instruction <= "1"&"000001"&"000100"&"0100"&"000000"&"000000011";        --reg1 COMP 2 = less => reg4 = 0    //long delay!!!
wait for 2000ns;

tb_instruction <= "0"&"000010"&"000011"&"0100"&"000001"&"000000000";        --reg2 COMP reg1 = great => reg3 = 0 //long delay!!
wait for 2000ns;

tb_instruction <= "0"&"000100"&"000101"&"0101"&"000010"&"000000000";        --reg4 AND reg2 => reg5
wait for 800ns;

tb_instruction <= "1"&"000100"&"000110"&"0101"&"111110"&"111111110";        --reg4 AND "510" => reg6
wait for 800ns;

tb_instruction <= "0"&"000011"&"000111"&"0110"&"000100"&"000000000";        --reg3 OR reg4 => reg7
wait for 800ns;

tb_instruction <= "1"&"000011"&"001000"&"0110"&"111110"&"111111110";        --reg4 OR "510" => reg8
wait for 800ns;

tb_instruction <= "0"&"001000"&"001001"&"0111"&"000000"&"000000000";        --NOT reg8 => reg9
wait for 800ns;

tb_instruction <= "0"&"000100"&"001010"&"1000"&"001000"&"000000000";     --reg4 XOR reg8 => reg10
wait for 800ns;

tb_instruction <= "1"&"000100"&"001011"&"1000"&"000001"&"000000001";        --reg4 XOR rt_imm => reg11
wait for 800ns;

tb_instruction <= "0"&"000100"&"001100"&"1001"&"000000"&"000000010";        --LSL2 reg4 => reg12, OP =0
wait for 800ns;

tb_instruction <= "0"&"000100"&"001101"&"1001"&"000000"&"000000010";        --LSL2 reg4 => reg13, OP =1
wait for 800ns;

tb_instruction <= "0"&"000100"&"001110"&"1010"&"000000"&"000000010";        --LSR2 reg4 => reg14, OP =0
wait for 800ns;

tb_instruction <= "1"&"000100"&"001111"&"1010"&"000000"&"000000010";        --LSR2 reg4 => reg15, OP =1
wait for 800ns;

tb_instruction <= "1"&"000000"&"010000"&"1101"&"111100"&"000000000";        --MOVI: -1024 => reg16      --give negative number to check arithmetic shifts later on
wait for 800ns;

tb_instruction <= "0"&"000100"&"010001"&"1101"&"000000"&"000000000";        --MOV reg4 => reg17
wait for 800ns;

tb_instruction <= "0"&"010000"&"010010"&"1011"&"000000"&"000000010";        --ASL2 reg16 => reg18   OP=0
wait for 800ns;

tb_instruction <= "1"&"010000"&"010011"&"1011"&"000000"&"000000010";        --ASL2 reg16 => reg19   OP =1
wait for 800ns;

tb_instruction <= "0"&"010000"&"010100"&"1100"&"000000"&"000000010";        --ASR2 reg16 => reg18   OP = 0
wait for 800ns;

tb_instruction <= "1"&"010000"&"010101"&"1100"&"000000"&"000000010";        --ASR2 reg16 => reg18   OP = 1
wait for 800ns;

tb_instruction <= "1"&"010000"&"010101"&"0000"&"000000"&"000000010";        --no operation
wait  for 800ns;

end process;
end control_arch_testbench;