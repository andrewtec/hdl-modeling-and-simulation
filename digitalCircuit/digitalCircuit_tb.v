module q2_tb;
reg a_tb, b_tb, c_tb;
wire z_tb;

q2 dut (.a(a_tb), .b(b_tb), .c(c_tb), .z(z_tb));

initial 
begin
    a_tb=1'b1;
    b_tb=1'b0;
    c_tb=1'b0;
    //1
end

always
begin
    a_tb=1'b1;
    b_tb=1'b0;
    c_tb=1'b0;
    #50 //1
    
    b_tb=1'b1;
    #50 //2
    
    c_tb=1'b1;
    #50 //3   

    b_tb=1'b0;
    #50 //4

    c_tb=1'b0;
    #50 //5
    
    a_tb=1'b0;
    #50 //6

    b_tb=1'b1;
    #50
    
    a_tb=1'b1;
    b_tb=1'b0;
    
end
endmodule
