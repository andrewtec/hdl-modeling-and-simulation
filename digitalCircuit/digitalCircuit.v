module q2 (a,b,c,z);
    input a, b, c;
    output z;
    
wire x, y;

assign #11 x = ~(a & b);
assign #8 y = x | c;
assign #19 z = ((~y) & a) | ((~a) & y);

endmodule