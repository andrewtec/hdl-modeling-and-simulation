library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity BLevel_dataflow_tb is
end BLevel_dataflow_tb;

architecture Beh_BLevel_dataflow of BLevel_dataflow_tb is

signal in3_tb, in2_tb, in1_tb, in0_tb, warn_tb : std_logic;

component BLevel_dataflow
    PORT( Indicator3 : in std_logic; Indicator2 : in std_logic; Indicator1 : in std_logic; Indicator0 : in std_logic; warning : out std_logic);
end component;
begin 
Instant1: BLevel_dataflow port map ( Indicator3 => in3_tb, Indicator2 => in2_tb, Indicator1 => in1_tb, Indicator0 => in0_tb, warning => warn_tb );

process
begin

    in3_tb <= '0';
    in2_tb <= '0';
    in1_tb <= '0';
    in0_tb <= '0';    
    wait for 50 ns;
    
    in3_tb <= '0';
    in2_tb <= '0';
    in1_tb <= '0';
    in0_tb <= '1';    
    wait for 50 ns;
    
    in3_tb <= '0';
    in2_tb <= '0';
    in1_tb <= '1';
    in0_tb <= '0';    
    wait for 50 ns;
    
    in3_tb <= '0';
    in2_tb <= '0';
    in1_tb <= '1';
    in0_tb <= '1';    
    wait for 50 ns;
        
    in3_tb <= '0';
    in2_tb <= '1';
    in1_tb <= '0';
    in0_tb <= '0';    
    wait for 50 ns;
    
    in3_tb <= '0';
    in2_tb <= '1';
    in1_tb <= '0';
    in0_tb <= '1';    
    wait for 50 ns;
    
    in3_tb <= '0';
    in2_tb <= '1';
    in1_tb <= '1';
    in0_tb <= '0';    
    wait for 50 ns;
    
    in3_tb <= '0';
    in2_tb <= '1';
    in1_tb <= '1';
    in0_tb <= '1';    
    wait for 50 ns; 
    
 ------------------------- 
        
    in3_tb <= '1';
    in2_tb <= '0';
    in1_tb <= '0';
    in0_tb <= '0';    
    wait for 50 ns;
    
    in3_tb <= '1';
    in2_tb <= '0';
    in1_tb <= '0';
    in0_tb <= '1';    
    wait for 50 ns;
    
    in3_tb <= '1';
    in2_tb <= '0';
    in1_tb <= '1';
    in0_tb <= '0';    
    wait for 50 ns;
    
    in3_tb <= '1';
    in2_tb <= '0';
    in1_tb <= '1';
    in0_tb <= '1';    
    wait for 50 ns;
        
    in3_tb <= '1';
    in2_tb <= '1';
    in1_tb <= '0';
    in0_tb <= '0';    
    wait for 50 ns;
    
    in3_tb <= '1';
    in2_tb <= '1';
    in1_tb <= '0';
    in0_tb <= '1';    
    wait for 50 ns;
    
    in3_tb <= '1';
    in2_tb <= '1';
    in1_tb <= '1';
    in0_tb <= '0';    
    wait for 50 ns;
    
    in3_tb <= '1';
    in2_tb <= '1';
    in1_tb <= '1';
    in0_tb <= '1';
    wait for 50ns;    
    
end process;
end Beh_BLevel_dataflow;