library ieee;
use ieee.std_logic_1164.all;

entity test_bench is
end test_bench;

architecture  test  of test_bench  is
signal in0_tb, in1_tb, c_tb,s_tb : bit;
component  Half_adder 	--This part is exactly same as body of half_adder entity
	port(a: in bit; b : in bit; S : out bit; C: 	out bit);
end component;

begin
-- Instantiating predefined components
instant1: Half_adder port map (a=>in0_tb,b=>in1_tb, C=>c_tb,S=>s_tb);

process
begin
	
	in0_tb <= '0';
	in1_tb <= '0';
	wait for 20 ns;
	in0_tb <= '1';
	in1_tb <= '0';
	wait for 10 ns;
	in0_tb <= '0';
	in1_tb <= '1';
	wait for 10 ns;
	in0_tb <= '1';
	in1_tb <= '1';
		

end process;
end test;
