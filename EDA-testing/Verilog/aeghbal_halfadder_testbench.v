
module testbench();
reg in0_tb,in1_tb;
wire s_tb,c_tb;

Half_adder instant (.a(in0_tb),.b(in1_tb),.S(s_tb),.C(c_tb));

initial 
begin
	in0_tb=1'b0;
	in1_tb=1'b0;
	#10
     	in0_tb=1'b0;
	in1_tb=1'b1;
	#10
     	in0_tb=1'b1;
	in1_tb=1'b0;
	#10
     	in0_tb=1'b1;
	in1_tb=1'b1;
end

endmodule
