----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/10/2016 04:23:32 PM
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsighed.ALL;

entity counter is
  Port ( clk : in STD_LOGIC;
         rst : in STD_LOGIC;
         counter_out : out  STD_LOGIC_VECTOR ( 4 downto 0);
end counter;

architecture Behavioral of counter is
    signal count_int : std_logic_vector (4 downto 0) := b"00000";
begin
    process ( clk, rst)
    if rst='1' then
        count_int <= b"00000";
    else if clk="1" and clk'event then
            count_int <= count_int+b"00001";
        end if;
    end if;
    end process;
counter_out <= count_int;
end Behavioral;
