library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity orgate is
Port ( IN1 : in STD_LOGIC;
	IN2 : in STD_LOGIC;
	OUT1 : out STD_LOGIC);
end orgate;

architecture Behavioral of orgate is
begin
OUT1 <= IN1 or IN2;
end Behavioral;