library ieee;
use ieee.std_logic_1164.all;

entity Half_adder is
	port(a: in bit; b : in bit; S : out bit; C: out bit);
end Half_adder;

architecture data_flow_half_adder of Half_adder is
begin
s<= a xor b;
c<= a and b;
end data_flow_half_adder;