-----------------------> Code by: Andrew Tec<---------------------------------
------------------->University of California, Irvine<----------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY Count_ones IS
    GENERIC (N: INTEGER := 3); -- number of bits
    PORT ( x: IN BIT_VECTOR (N -1 DOWNTO 0); 
                  y: OUT NATURAL RANGE 0 TO N
                  );
END ENTITY ;

architecture beh_ones_counter of Count_ones is

type natural_array is array (N-1 downto 0) of natural;      --have an array of natural numbers
signal count : natural_array;

begin

-----------emulates using adders and selectors ( "+", "when", and "else")--------------

count(0) <= 1 when x(0) ='1' else 0;                                                               --handles the first cell; when there is a 1 in the first cell at cout(0), put a 1

    incrementOrCarry_LeadingCellOfCount: for i in 1 to N-1 generate
                count(i) <= count(i-1) +1 when                                                       --give leading cell an increment of the cell before it
                    x(i) = '1'  else                                                                                  --only when there is a value of 1 in signal x
                    count(i-1) +0;                                                                                 --else don't increment leading cell, just "carry over" value from previous cell
    end generate incrementOrCarry_LeadingCellOfCount;
        
y <= count(N-1) ;     --read only the leading cell value
        
end architecture beh_ones_counter;