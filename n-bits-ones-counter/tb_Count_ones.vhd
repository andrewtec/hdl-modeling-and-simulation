library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_countOnes is
end tb_countOnes;

architecture beh_countOnes of tb_countOnes is

constant EIGHTBITS : integer := 8;
constant THREEBITS : integer := 3;
constant TENBITS : integer := 10;

signal tb_a : bit_vector (EIGHTBITS-1 downto 0);
signal tb_b : natural RANGE 0 to EIGHTBITS;

signal tb_c : bit_vector (THREEBITS-1 downto 0);
signal tb_d : natural RANGE 0 to THREEBITS;

signal tb_e : bit_vector (TENBITS-1 downto 0);
signal tb_f : natural RANGE 0 to TENBITS;

component Count_ones is
    GENERIC (N : INTEGER);
    PORT(  
     x: IN BIT_VECTOR (N -1 DOWNTO 0); 
     y: OUT NATURAL RANGE 0 TO N
     );
end component;

begin

eightBitswide: Count_ones
    generic map (N => EIGHTBITS)
    port map ( tb_a, tb_b);

    process
    begin
    tb_a <= "11110000";
wait for 10ns;
    tb_a <= "00010101";
wait for 10ns;
end process;

threeBitswide: Count_ones
    generic map (N => THREEBITS)
    port map ( tb_c, tb_d);

    process
    begin
    tb_c <= "111";
wait for 10ns;
    tb_c <= "001";
wait for 10ns;
end process;

tenBitswide: Count_ones
    generic map (N => TENBITS)
    port map ( tb_e, tb_f);

    process
    begin
    tb_e <= "0000000001";
wait for 10ns;
    tb_e <= "1111111110";
wait for 10ns;
end process;

end architecture beh_countOnes;