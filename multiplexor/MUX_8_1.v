/**************** Andrew Tec,  University of California, Irvine ***************/
//****************8X1 SELECTOR w/ 2X1 SELECTORS****************//
module MUX_8_1 (i0 ,i1 ,i2 ,i3 ,i4 ,i5 ,i6 ,i7 , selectors , o_8 );
    input [31:0] i0 ,i1 ,i2 ,i3 ,i4 ,i5 ,i6 ,i7;
    input [2:0] selectors ;             //3 bit selector
    output [31:0] o_8 ;

wire [31:0] net0, net1, net2, net3, net4, net5;        //6 32bit net outputs for 2X1 selectors

begin
//first four 2_1 MUX take in signals
MUX_2_1 firstpair_in (
    .i0(i0),
    .i1(i1),
    .sel(selectors[0]),
    .o(net0)
    );
 
 MUX_2_1 secondpair_in (
    .i0(i2),
    .i1(i3),
    .sel(selectors[0]),
    .o(net1)
    );
               
MUX_2_1 thirdpair_in (
    .i0(i4),
    .i1(i5),
    .sel(selectors[0]),
    .o(net2)
    );    

MUX_2_1 fourthpair_in(
    .i0(i6),
    .i1(i7),
    .sel(selectors[0]),
    .o(net3)
    );
//end taking in inputs

//begin taking in inputs from other seletors
MUX_2_1 fifthpair_net1(
    .i0(net0),
    .i1(net1),
    .sel(selectors[1]),
    .o(net4)
    );
    
 MUX_2_1 sixthpair_net2(
        .i0(net2),
        .i1(net3),
        .sel(selectors[1]),
        .o(net5)
        );   
 
 //final decision       
 MUX_2_1 finalpair( 
        .i0(net4),
        .i1(net5),
        .sel(selectors[2]),
        .o(o_8)
        ); 
end   
//end taking in selector inputs
endmodule