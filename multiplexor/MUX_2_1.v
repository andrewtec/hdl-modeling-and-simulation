/**************** Andrew Tec,  University of California, Irvine ***************/
/*****************2X1 SELECTOR*****************/
module MUX_2_1 (i0 ,i1 ,sel ,o);
    input [31:0] i0 ,i1;        //32 bit input
    input sel ;                     //single bit input
    output [31:0] o;            //32 bit output

wire  [31:0]  f, g;

genvar i;    
generate 
for (i=0; i <= 31; i = i+1)         //go through every bit, not just LSB
    begin
    
        assign #11 g[i] = ~(sel) & i0[i]  ;   //AND is 8ns, NOT is 3ns
        assign #8 f[i] = sel & i1[i]  ;         //AND is 8ns
        assign #8 o[i] = f[i] | g[i];               //OR is 8ns
    end
endgenerate
//sel =0 gives i0 (the right), sel = 1 gives i1 (the left)
endmodule
