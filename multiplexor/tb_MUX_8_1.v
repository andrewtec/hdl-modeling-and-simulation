module tb_MUX_8_1;
    reg [31:0] in0, in1, in2, in3, in4, in5, in6, in7;
    reg [2:0] select_bits;
    wire [31:0] the_selected;
    
 MUX_8_1  dut (
    .i0(in0 [31:0] ),
    .i1(in1 [31:0] ),
    .i2(in2 [31:0] ),
    .i3(in3 [31:0] ),
    .i4(in4 [31:0] ),
    .i5(in5 [31:0] ),
    .i6(in6 [31:0] ),
    .i7(in7 [31:0] ),
    .selectors(select_bits[2:0]),
    .o_8(the_selected [31:0] )    
);

initial
begin
    in0 = 32;
    in1 = 31;
    in2 = 30;
    in3 = 29;
    in4 = 28;
    in5 = 27;
    in6 = 26;
    in7 = 25;
    select_bits = 3'b000;
    #200
   
    select_bits = 3'b001;
   #200
   
   select_bits = 3'b010;     
   #200
   
   select_bits = 3'b011;     
   #200
   
   select_bits = 3'b100;     
   #200
   
   select_bits = 3'b101;     
   #200
   
   select_bits = 3'b110;     
   #200   

   select_bits = 3'b111;     

end
endmodule
