-----------------------> Code by: Andrew Tec<---------------------------------
------------------->University of California, Irvine<----------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_shifter_25381671 is
end tb_shifter_25381671;

architecture Behavioral of tb_shifter_25381671 is

constant THIRTYTWO : integer := 32;
signal tb_i, tb_o: std_logic_vector (THIRTYTWO-1 downto 0);
signal tb_dir, tb_AL : bit;
signal tb_shifting : integer range 0  to THIRTYTWO-1;


component shifter is
GENERIC (BW : INTEGER :=32);
PORT ( i : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
    dir : IN BIT; -- dir ='0' means shift right and dir ='1' means shift left
    AL: IN BIT ; -- Arithmetic or logical shift asume AL='1' means arithmetic an AL ='0' means logical
    shifting : IN INTEGER range 0 to BW -1;
    o: OUT STD_LOGIC_VECTOR (BW -1 DOWNTO 0)
    );
end component;


begin
forward: shifter
    generic map (BW => THIRTYTWO)
    port map ( tb_i, tb_dir, tb_AL, tb_shifting, tb_o);

process
begin
    tb_i <= "11111111111111111111111111111111";
    tb_dir <= '0';      --SR
    tb_AL <= '0';       --logical
    tb_shifting <= 31;
    wait for 1000ns;
    
    tb_i <= "11111111111111111111111111111111";
    tb_dir <= '1';      --SL
    tb_AL <= '0';       --logical
    tb_shifting <= 31;
    wait for 1000ns;
    
    tb_i <= "11111111111111111111111111111110";
    tb_dir <= '1';      --SL
    tb_AL <= '1';       --arith
    tb_shifting <= 5;
    wait for 1000ns;
    
    tb_i <= "01111111111111111111111111111111";
    tb_dir <= '1';      --SR
    tb_AL <= '1';       --arith
    tb_shifting <= 5;
    wait for 1000ns;
    
    tb_i <= "11111111111111111111111111111111";
    tb_dir <= '1';      --SR
    tb_AL <= '1';       --arith
    tb_shifting <= 0;       --NO SHIFT
    wait for 1000ns;
    
    
end process;
end Behavioral;
