-----------------------> Code by: Andrew Tec<---------------------------------
------------------->University of California, Irvine<----------------------

library ieee;
use ieee.std_logic_1164.all;

ENTITY shifter is
GENERIC (BW : INTEGER :=32);
PORT ( i : IN STD_LOGIC_VECTOR (BW -1 DOWNTO 0);
    dir : IN BIT; -- dir ='0' means shift right and dir ='1' means shift left
    AL: IN BIT ; -- Arithmetic or logical shift asume AL='1' means arithmetic an AL ='0' means logical
    shifting : IN INTEGER range 0 to BW -1;
    o: OUT STD_LOGIC_VECTOR (BW -1 DOWNTO 0)
    );
END ;

ARCHITECTURE shifter_arch OF shifter IS

signal poss : bit_vector (1 downto 0);
signal fill : std_logic_vector (BW-1 downto 0);

BEGIN
poss <= dir & AL;

--"dynamic" filling on both ends for all cases
concatenate: for k in 1 to BW-1 generate
    o <= i(BW-1 downto 0) when shifting = 0 else
            fill(k-1 downto 0) & i(BW-1 downto k) after k*10ns when ((poss = "00") and (shifting = k)) else   -- logic right shift
            fill (k-1 downto 0) & i(BW-1 downto k) after k*10ns when ((poss = "01") and (shifting = k)) else    -- arithmetic right shift
            i(BW-1-k downto 0) & fill(k-1 downto 0) after k*10ns  when ((poss = "10") and (shifting = k)) else    -- logic left shift
            i(BW-1-k downto 0) & fill(k-1 downto 0) after k*10ns  when ((poss = "11") and (shifting = k))     --arithmetic left shift
            else (others => 'Z' );    ---cover any other value possible
end generate;

--give values to dynamic filling
with poss select
        fill <= (others => '0') when "00",  --logical
            (others => '0') when "10",      --logical
            (others => i(BW-1)) when "01",      --arith
            (others => i(0)) when "11";     --arith
            
END ARCHITECTURE ;