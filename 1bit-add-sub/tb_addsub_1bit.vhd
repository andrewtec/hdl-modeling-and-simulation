library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tb_addsub_1bit is
end tb_addsub_1bit;

architecture beh_addsub_1bit of tb_addsub_1bit is
signal i0, i1, carryin, addsub, answer, carryout_orborrow : std_logic;

component addsub_1bit
    PORT ( in_0 : IN std_logic ;
        in_1 : IN std_logic ;
        cin : IN std_logic ;
        AddOrSub : IN std_logic ; -- decide to add ( '1 ') or sub ( '0 ') operation
        sum_sub : OUT std_logic ; -- if sub operation is chosen it will be sub
        cout_bout : OUT std_logic -- if sub operation is chosen it will be borrow out
        ); 
end component;
begin
PortForward: addsub_1bit port map ( in_0 => i0, in_1 => i1, 
                                    cin => carryin, AddOrSub => addsub, 
                                    sum_sub => answer, cout_bout => carryout_orborrow);

process

begin
------------------ test adding NO CARRYIN
    addsub <= '1'; 
    carryin <= '0';
    i0 <= '0';
    i1 <='0';
    wait for 50ns;
    
    addsub <= '1';
    carryin <= '0';
    i0 <= '1';
    i1 <='0';
    wait for 50ns;
    
    addsub <= '1';
    carryin <= '0';
    i0 <= '0';
    i1 <='1';
    wait for 50ns;
        
     addsub <= '1';
    carryin <= '0';
    i0 <= '1';
    i1 <='1';
    wait for 50ns; ---200ns

------------------- test adding WITH CARRYIN
    addsub <= '1'; 
    carryin <= '1';
    i0 <= '0';
    i1 <='0';
    wait for 50ns;
    
    addsub <= '1';
    carryin <= '1';
    i0 <= '1';
    i1 <='0';
    wait for 50ns;

    addsub <= '1';
    carryin <= '1';
    i0 <= '0';
    i1 <='1';
    wait for 50ns;

    addsub <= '1';
    carryin <= '1';
    i0 <= '1';
    i1 <='1';
    wait for 50ns; ---400ns
    
----------------------- test subtracting NO BORROWIN
    addsub <= '0';
    carryin <= '0';
    i0 <= '0';
    i1 <='0';
    wait for 50ns;
    
    addsub <= '0';
    carryin <= '0';
    i0 <= '1';
    i1 <='0';
    wait for 50ns;    
    
    addsub <= '0';
    carryin <= '0';
    i0 <= '0';
    i1 <='1';
    wait for 50ns;    
    
    addsub <= '0';
    carryin <= '0';
    i0 <= '1';
    i1 <='1';
    wait for 50ns; ---600ns

--------------------test subtract WITH BORROWIN
    addsub <= '0';
    carryin <= '1';
    i0 <= '0';
    i1 <='0';
    wait for 50ns;    

    addsub <= '0';
    carryin <= '1';
    i0 <= '1';
    i1 <='0';
    wait for 50ns;    

    addsub <= '0';
    carryin <= '1';
    i0 <= '0';
    i1 <='1';
    wait for 50ns;    

    addsub <= '0';
    carryin <= '1';
    i0 <= '1';
    i1 <='1';   ---800ns
    
end process;
end beh_addsub_1bit;